# HH scraper

## Использование

1) Откройте страницу "Управление" и откройте страницу поиска на HH. **Важно: каждая из этих страниц должна быть открыта в единственном экземпляре и не должна закрываться во время процесса!**

2) **Перед тем как выполнять какие-либо действия дожидайтесь полной загрузки любых страниц.**

3) Управляйте парсингом с помощью кнопок управления и настроек.

## Кнопки управления

- **Старт**: начинает парсинг. Если поставлено на паузу, то возобновляет парсинг;
- **Пауза**: ставит парсинг на паузу;
- **Стоп**: останавливает парсинг.

## Настройки

- Описание настроек можно прочитать наведя курсор на знак вопроса;
- Настройки сохраняются и обновляются без каких-либо дополнительных действий (то есть не нужно обновлять страницы для применения настроек или нажимать на кнопку "сохранить").

## Ограничение по размеру

Расширение не тестировалось для парсинга большого количества данных. Вполне возможно, что максимальный размер файла не может превышать 500 MiB.

## Чтобы получать контактные данные, нужно вставить селекторы

Файл – `/interaction/js/content/common/parser.js`

Строки – `78`, `89`, `90`
