"use strict";


/**
 * Управление событиями расширения.
 */
class Events {
    /**
     * Вызывается после установки расширения.
     * 
     * @static
     */
    static onInstall() {
        STORAGE_SYNC.setInitialSettings();
    }

    /**
     * Вызывается после обновления расширения.
     * 
     * @static
     */
    static onUpdate() {}
}


chrome.runtime.onInstalled.addListener((details) => {
    if (details.reason === "install") {
        Events.onInstall();
    } else if (details.reason === "update") {
        Events.onUpdate();
    }
});
