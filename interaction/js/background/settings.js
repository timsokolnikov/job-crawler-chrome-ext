"use strict";


/**
 * Информация о профиле.
 */
class ProfileInfo {
    static get defaultSettings() {
        return {
            // Открывать страницы в новых вкладках
            openPagesInNewTab: false,
            // Время между открытием страниц 
            timeBetweenPageOpening: 0,
            // Многопоточность
            multithread: false,
            // Количество потоков 
            threadCount: 1,
            // Пагинация
            pagination: false,
            // Проверка на дубликаты
            checkForDuplicate: true,
            // Имя файла
            fileName: ""
        };
    }
}


/**
 * Управляет пользовательскими настройками, которые хранятся в storage sync.
 */
class StorageSync {
    /**
     * Получает информацию из хранилища по ключу.
     * 
     * @static
     * @async
     *
     * @param {string | string[] | Object | null} key Ключ настроек.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage/StorageArea/get
     */
    static get(key) {
        return new Promise((resolve) => {
            chrome.storage.sync.get(key, (items) => {
                return resolve(items);
            });
        });
    }

    /**
     * Сохраняет информацию в хранилище.
     * 
     * @static
     * @async
     *
     * @param {Object} items Информация для сохранения.
     * 
     * @see https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage/StorageArea/set
     */
    static set(items) {
        return new Promise((resolve) => {
            chrome.storage.sync.set(items, () => {
                return resolve();
            });
        });
    }

    /**
     * Очищает хранилище sync.
     * 
     * @static
     * @async
     * 
     * @returns {Promise<void>} 
     * Promise, который будет выполнен, когда хранилище очистится.
     */
    static clear() {
        return new Promise((resolve) => {
            chrome.storage.sync.clear(() => {
                return resolve();
            });
        });
    }

    /**
     * Очищает хранилище и устанавливает настройки по умолчанию.
     * 
     * @static
     * @async
     */
    static async setInitialSettings() {
        await this.clear();
        await this.set(ProfileInfo.defaultSettings);
    }
}


/**
 * Управляет пользовательскими настройками, которые хранятся в storage sync.
 * 
 * Смотрите документацию `StorageSync` для подробностей.
 */
const STORAGE_SYNC = (() => {
    return StorageSync;
})();
