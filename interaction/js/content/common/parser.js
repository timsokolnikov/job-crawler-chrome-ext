"use strict";


/**
 * Выполняет парсинг страницы.
 * 
 * Разработан для страниц `*://*.hh.ru/search/*` и `*://*.hh.ru/resume/*`.
 */
class Parser {
    /**
     * Собирает данные о резюме со страницы поиска.
     * 
     * Разработан для `*://*.hh.ru/search/*`.
     * 
     * @static
     * 
     * @returns {Array<{url: string}>} Массив объектов. Объект содержит: URL.
     */
    static getResumesData() {
        const resumes = document.querySelectorAll(`div.resume-search-item[itemtype="http://schema.org/Person"]`);

        if (!resumes) {
            console.warn("Не удалось найти резюме.");
            return;
        }

        const resumeData = [];

        for (let resume of resumes) {
            const resumeItem = {};

            resumeItem.url = resume.querySelector(`a.search-item-name[itemprop="jobTitle"]`).href;

            resumeData.push(resumeItem);
        }

        return resumeData;
    }

    /**
     * Собирает полные данные о резюме со страницы резюме.
     * 
     * Разработан для `*://*.hh.ru/resume/*`.
     * 
     * @static
     * 
     * @param {document} dcmnt 
     * Документ, на котором будет производиться парсинг.
     */
    static getResumeData(dcmnt) {
        const get = (selector) => {
            // `.replace(/\n/gi, " ")` не работает для страниц, которые не были отрендерины 
            // (то есть если они загружены через скрытый iframe).
            return this._get(dcmnt, selector).replace(/\n/gi, " ");
        };

        /**
         * Русскоязычные ключи, потому что в будущем 
         * эти данные будут сохраняться в .xlsx.
         * Будут сохранены точно в таком же порядке,
         * в котором они созданы здесь.
         */
        const data = {
            "ФИО": "",
            "Пол": "",
            "Возраст": "",
            "Город": "",
            "Желаемая должность": "",
            "Желаемая зарплата": "",
            "Опыт работы": "",
            "Образование": "",
            "Ключевые навыки": "",
            "Знание языков": "",
            "Обо мне": "",
            "Телефон": "",
            "Почта": "",
            "URL": ""
        };
        
        //data["ФИО"] = get(/*`[селектор, похожий на data-qa=*]`*/);
        data["Пол"] = get(`[data-qa="resume-personal-gender"]`);
        data["Возраст"] = get(`[data-qa="resume-personal-age"]`);
        data["Город"] = get(`[data-qa="resume-personal-address"]`);
        data["Желаемая должность"] = get(`[data-qa="resume-block-title-position"]`);
        data["Желаемая зарплата"] = get(`[data-qa="resume-block-salary"]`);
        data["Опыт работы"] = get(`[data-qa="resume-block-experience"]`).replace("Опыт работы", "").replace(/... Показать еще/g, " ").trim();
        data["Образование"] = get(`[data-qa="resume-block-education"]`);
        data["Ключевые навыки"] = get(`[data-qa="skills-table"]`).replace("Ключевые навыки", "").replace("Теперь резюме открыто всему интернету — изменить можно в настройках видимости.", "").replace("Возникли неполадки. Попробуйте еще раз.", "").trim();
        data["Знание языков"] = get(`[data-qa="resume-block-languages"]`).replace("Знание языков", "").trim();
        data["Обо мне"] = get(`[data-qa="resume-block-skills"]`);
        //data["Телефон"] = get(/*`[селектор, похожий на data-qa=*]`*/);
        //data["Почта"] = get(/*`[селектор, похожий на data-qa=*]`*/);
        data["URL"] = dcmnt.location.href;

        return data;
    }

    /**
     * Получает текстовое значение элемента.
     * 
     * @static
     * 
     * @param {document} dcmnt Документ, в котором будет осуществляться поиск.
     * @param {string} selector Селектор, по которому будет осуществляться поиск.
     * 
     * @returns {string} Если элемент найден, то его текстовое значение, иначе пустая строка.
     */
    static _get(dcmnt, selector) {
        const element = dcmnt.querySelector(selector);
        return element ? element.innerText : "";
    }
}


/**
 * Парсер страницы.
 * 
 * Смотрите документацию `Parser` для подробностей.
 */
const PARSER = (() => {
    return Parser;
})();
