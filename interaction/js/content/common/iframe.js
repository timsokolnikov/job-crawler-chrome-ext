"use strict";


/**
 * Рендеринг других страниц через iframe элемент.
 */
class Iframe {
    /**
     * Создает экземпляр `Iframe`.
     * 
     * @param {document} dcmnt 
     * Документ, с помощью которого будет создан элемент iframe. 
     */
    constructor(dcmnt) {
        this._iframe = dcmnt.createElement("iframe");

        this._iframe.id = this._generateHash();
        this._iframe.name = `render-iframe.${this._generateHash()}`;
        this._iframe.title = `render-iframe.${this._generateHash()}`;
        this._iframe.style.display = "none";
        this._iframe.height = "0";
        this._iframe.width = "0";
        this._iframe.allowFullscreen = false;
        this._iframe.allowPaymentRequest = false;

        return this;
    }

    /**
     * @returns {HTMLIFrameElement} iframe элемент.
     * @throws Бросает ошибку, если iframe элемент не инициализирован.
     */
    get iframe() {
        if (!this._iframe) {
            throw new Error("Iframe элемент не инициализирован.");
        }

        return this._iframe;
    }

    /**
     * @returns {document} document iframe элемента. 
     */
    get document() {
        return this.iframe.contentWindow.document;
    }

    /**
     * @returns {HTMLBodyElement} body iframe элемента.
     */
    get body() {
        return this.document.body;
    }

    /**
     * Устанавливает URL в iframe и начинает загрузку страницы.
     * 
     * @async
     * 
     * @param {string} url 
     * URL для установки.
     * 
     * @returns {Promise<void>} 
     * Promise, который будет выполнен, когда страница загрузится.
     */
    src(url) {
        this.iframe.src = url;

        return new Promise((resolve) => {
            this.iframe.onload = () => {
                return resolve();
            };
        });
    }

    /**
     * Очищает iframe, устанавливая адрес `about:blank`.
     * 
     * @async
     * 
     * @returns {Promise<void>} 
     * Promise, который будет выполнен, когда страница загрузится.
     */
    clear() {
        return this.src("about:blank");
    }

    /**
     * Удаляет iframe из родительского элемента.
     */
    delete() {
        this.iframe.parentNode.removeChild(this.iframe);
    }

    /**
     * Прикрепляет iframe к элементу.
     * 
     * @param {HTMLElement} element Родительский элемент.
     */
    attachTo(element) {
        element.appendChild(this.iframe);
    }

    /**
     * Генерирует рандомный хэш.
     * 
     * @example "4yr4dfxvvr"
     */
    _generateHash() {
        return (Math.random() + 1).toString(36).slice(2);
    }
}


/**
 * Iframe для рендеринга страниц.
 * 
 * Смотрите документацию `Iframe` для подробностей.
 */
const RENDER_IFRAME = (() => {
    return Iframe;
})();
