"use strict";


/**
 * Обработчик сообщений.
 * 
 * Может обрабатывать сообщения следующих типов: "command".
 */
class ContentMessage {
    /**
     * Обрабатывает все сообщения и 
     * перенаправляет их по `type`.
     * 
     * @static
     * 
     * @see https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage#Parameters
     */
    static onMessage(request, sender, sendResponse) {
        switch (request.type) {
            case "command": {
                this.commandHandler(request, sender, sendResponse);
                break;
            }
        
            default: {
                console.warn(request);
                console.warn(sender);
                throw new Error("Неизвестный тип сообщения.");
            }
        }
    }

    /**
     * Обрабатывает сообщения типа `command`.
     * 
     * @static
     */
    static commandHandler(request, sender, sendResponse) {
        switch (request.command) {
            case "startParsing": {
                MAIN.start();
                break;
            }

            case "pauseParsing": {
                MAIN.pause();
                break;
            }

            case "resumeParsing": {
                MAIN.resume();
                break;
            }

            case "stopParsing": {
                MAIN.stop();
                break;
            }

            case "updateSettings": {
                SETTINGS.init();
                break;
            }

            case "newTabWasParsedCanContinue": {
                MAIN.newTabWasParsedCanContinue();
                break;
            }
        
            default: {
                console.warn(request);
                console.warn(sender);
                throw new Error("Неизвестная команда.");
            }
        }

        // посылает ответ (на данный момент только в /interface/control.js), 
        // что сообщение получено и можно продолжать работу.
        // не обрабатывает ошибки в MAIN.
        sendResponse({status: true});
    }
}


chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    ContentMessage.onMessage(request, sender, sendResponse);
    return true; // асинхронный ответ.
});
