"use strict";


/**
 * Пользовательские настройки.
 * Смотрите пример в `/interaction/background/settings.js`.
 */
let USER_SETTINGS = {};

/**
 * Контролирует состояние паузы во
 * время цикла парсинга резюме.
 * 
 * Объект:
 * - state: текущий Promise.
 * - resolve: resolve функция для текущего Promise.
 */
let PAUSE_STATE = {
    state: Promise.resolve(),
    resolve: undefined
};

/**
 * Контролирует состояние досрочного
 * выхода из цикла парсинга резюме.
 * 
 * True - будет произведен досрочный выход,
 * False - досрочного выхода не будет.
 */
let FORCE_QUIT = false;

/**
 * Указывает, каким образом был совершен
 * переход на следующую страницу.
 * 
 * `True` - переход был совершен скриптом,
 * `False` - переход был совершен пользователем (или окно закрыто пользователем).
 */
let GO_TO_THE_NEXT_PAGE_PROGRAMMATICALLY = false;

/**
 * Контролирует состояние паузы во время 
 * открытия других вкладок для парсинга.
 * 
 * - promises: Promise, которые указывают, выполнен ли парсинг для открытых вкладок.
 * - resolves: resolve функции, с помощью которых можно выполнить Promise из promises.
 * - lastIndex: индекс последнего невыполненного Promise. 
 *              Может использоваться как и для promises, так и для resolves.
 */
let STATES_FROM_NEW_TABS = {
    promises: [],
    resolves: [],
    lastIndex: 0
};

/**
 * Возвращает контроллеры в исходное состояние.
 */
function resetControllers() {
    PAUSE_STATE.state = Promise.resolve();
    PAUSE_STATE.resolve = undefined;
    FORCE_QUIT = false;
    GO_TO_THE_NEXT_PAGE_PROGRAMMATICALLY = false;
    STATES_FROM_NEW_TABS.promises = [];
    STATES_FROM_NEW_TABS.resolves = [];
    STATES_FROM_NEW_TABS.lastIndex = 0;
}


/**
 * Управление пользовательскими настройками.
 */
class Settings {
    /**
     * Инициализирует настройки.
     * 
     * Получает настройки и устанавливает их в `USER_SETTINGS`.
     * 
     * @static
     * @async
     * 
     * @returns {Promise<void>}
     * Promise, который будет выполнен, когда настройки будут получены.
     */
    static init() {
        return new Promise((resolve) => {
            chrome.storage.sync.get(null, (items) => {
                USER_SETTINGS = items;
                return resolve();
            })
        });
    }
}


/**
 * Управляет процессом парсинга.
 */
class Main {
    /**
     * Запускается, когда страница имеет статус `DOMContentLoaded`.
     * 
     * Инициализирует настройки и отправляет сообщение, 
     * что страница готова для парсинга.
     *
     * @static
     * @async
     */
    static main() {
        Settings.init();
        chrome.runtime.sendMessage({type: "command", command: "readyForParsing"});
    }

    /**
     * Запускает парсинг.
     * 
     * По окончании отправляет сообщение, что парсинг завершен.
     * 
     * @static
     * @async
     * 
     * @throws Бросает ошибку, если не удалось найти резюме на странице поиска.
     */
    static async start() {
        const resumesData = PARSER.getResumesData();

        if (!resumesData.length) {
            chrome.runtime.sendMessage({type: "command", command: "parsingCompleted"});
            throw new Error("Не удалось найти резюме на странице поиска.");
        }

        /* Начало парсинга. */

        /**
         * Загрузка, парсинг и отправка данных в текущей вкладке.
         * 
         * @async
         * 
         * @param {string} url 
         * URL страницы резюме.
         * 
         * @returns {Promise<void>}
         * Promise, который будет выполнен, когда все это закончится.
         */
        function _load_in_current_tab(url) {
            return new Promise(async (resolve) => {
                const renderIframe = new RENDER_IFRAME(document);
                renderIframe.attachTo(document.body);

                await renderIframe.src(url);
                const data = PARSER.getResumeData(renderIframe.document);
                Main.sendData(data);

                await renderIframe.clear();
                renderIframe.delete();
            
                return resolve();
            });
        }

        /**
         * Загрузка, парсинг и отправка данных в новой вкладке.
         * 
         * @async
         * 
         * @param {string} url 
         * URL страницы резюме.
         */
        function _load_in_new_tab(url) {
            window.open(url, "_blank");

            let resolve = undefined;
            const promise = new Promise((rslv) => {
                resolve = rslv;
            });

            STATES_FROM_NEW_TABS.promises.push(promise);
            STATES_FROM_NEW_TABS.resolves.push(resolve);
        }

        while (resumesData.length) {
            // пауза.
            // должно стоять перед досрочным выходом.
            await PAUSE_STATE.state;

            // досрочный выход.
            if (FORCE_QUIT) {
                break;
            }

            /* Начало получения резюме для одновременной загрузки. */

            const resumeForLoad = [];

            if (USER_SETTINGS.multithread) {
                for (let index = 0; ((index < USER_SETTINGS.threadCount) && (resumesData.length)); index++) {
                    resumeForLoad.push(resumesData.shift());
                }
            } else {
                resumeForLoad.push(resumesData.shift());
            }

            /* Конец получения резюме для одновременной загрузки. */

            /* Начало обработки всех URL. */

            const promises = [];

            for (let resume of resumeForLoad) {
                if (USER_SETTINGS.openPagesInNewTab) {
                    _load_in_new_tab(resume.url);
                } else {
                    promises.push(_load_in_current_tab(resume.url));
                }
            }

            if (USER_SETTINGS.openPagesInNewTab) {
                await Promise.all(STATES_FROM_NEW_TABS.promises)
            } else {
                await Promise.all(promises);
            }

            /* Конец обработки всех URL. */

            // если остались резюме, то ждем определенное
            // время перед началом обработки оставшихся.
            if (resumesData.length) {
                await new Promise((resolve) => {
                    window.setTimeout(() => {
                        return resolve();
                    }, USER_SETTINGS.timeBetweenPageOpening);
                });
            }
        }

        /* Конец парсинга. */

        if (FORCE_QUIT) {
            chrome.runtime.sendMessage({type: "command", command: "parsingCompleted"});
        } else {
            if (USER_SETTINGS.pagination) {
                GO_TO_THE_NEXT_PAGE_PROGRAMMATICALLY = true;
                // клик на кнопку "Дальше".
                document.querySelector(`a[data-qa="pager-next"]`).click();
            } else {
                chrome.runtime.sendMessage({type: "command", command: "parsingCompleted"});
            }
        }

        resetControllers();
    }

    /**
     * Приостанавливает парсинг.
     * 
     * @static
     */
    static pause() {
        PAUSE_STATE.state = new Promise((resolve) => {
            PAUSE_STATE.resolve = resolve;
        });
    }

    /**
     * Возобновляет парсинг.
     * 
     * @static
     */
    static resume() {
        if (PAUSE_STATE.resolve) {
            PAUSE_STATE.resolve();
        }

        PAUSE_STATE.state = Promise.resolve();
        PAUSE_STATE.resolve = undefined;
    }

    /**
     * Завершает парсинг.
     * 
     * @static
     */
    static stop() {
        this.resume(); // на случай, если не сняли с паузы.
        FORCE_QUIT = true;
    }

    /**
     * Отправляет данные резюме.
     * 
     * @static
     * 
     * @param {Object} data Полные данные резюме для отправки.
     */
    static sendData(data) {
        chrome.runtime.sendMessage({
            type: "command", 
            command: "collectData", 
            data: data
        });
    }

    /**
     * Вызывается, когда новая открытая вкладка была обработана.
     * 
     * Выполняет действия, необходимые для продолжения парсинга других резюме.
     * 
     * @static
     */
    static newTabWasParsedCanContinue() {
        const index = STATES_FROM_NEW_TABS.lastIndex;
        const resolve = STATES_FROM_NEW_TABS.resolves[index];

        resolve(); // выполняем последний невыполненный promise.
        STATES_FROM_NEW_TABS.lastIndex++; // теперь ссылаемся на другой невыполненный promise.
    }
}


/**
 * Управляет процессом парсинга.
 * 
 * Смотрите документацию `Main` для подробностей.
 */
const MAIN = (() => {
    return Main;
})();

/**
 * Управление пользовательскими настройками.
 * 
 * Смотрите документацию `Settings` для подробностей.
 */
const SETTINGS = (() => {
    return Settings;
})();


/**
 * Добавляем событие для закрытия страницы.
 * 
 * Когда страницу закрывают или перезагружают,
 * то отправляет команду `detachParsing`.
 * 
 * Однако отправляет в том случае, если был принудительный выход
 * или окно было закрыто перед пагинацией.
 */
window.addEventListener("beforeunload", () => {
    if (FORCE_QUIT || !GO_TO_THE_NEXT_PAGE_PROGRAMMATICALLY) {
        chrome.runtime.sendMessage({type: "command", command: "detachParsing"});
    }
}, false);

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", Main.main);
} else {
    Main.main();
}
