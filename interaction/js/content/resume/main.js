/**
 * Запускается, когда страница имеет статус "DOMContentLoaded".
 * 
 * Парсит страницу и отправляет данные в окно контроля (control.html).
 * После всего этого закрывает вкладку.
 */
function main() {
    const data = PARSER.getResumeData(document);
    console.log(data);
    chrome.runtime.sendMessage({type: "command", command: "newTabWasParsed", data: data});
    //  window.close();
}


if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
} else {
    main();
}
