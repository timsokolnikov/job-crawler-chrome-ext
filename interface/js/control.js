"use strict";


/**
 * Здесь хранятся данные резюме.
 * 
 * Нужны для отображения в таблице и для сохранения.
 */
const DATA = [];

/**
 * Пользовательские настройки.
 * 
 * Смотрите пример в `/interaction/background/settings.js`.
 */
let USER_SETTINGS = {};

/**
 * Состояние парсинга.
 * 
 * `True` - парсинг включен,
 * `False` - парсинг выключен.
 */
let PARSING_IS_ENABLED = false;


/**
 * С помощью этого класса посылаются сообщения.
 */
class Message {
    /**
     * Посылает сообщение контент-скрипту (только на `*://*.hh.ru/search/*`).
     * 
     * @static
     * @async
     * 
     * @param {any} message 
     * Сообщение для отправки.
     * 
     * @param {boolean} alert
     * Показывать уведомление о том, если вкладка поиска не найдена.
     * По умолчанию `true`.
     * 
     * @returns {Promise<void>} 
     * Promise, который будет разрешен, когда получит ответ от контент-скрипта.
     * 
     * @throws
     * Бросает ошибку, если нет открытых вкладок с адресом `*://*.hh.ru/search/*`.
     */
    static sendMessage(message, alert) {
        if (alert === undefined) {
            alert = true;
        }

        return new Promise((resolve) => {
            chrome.tabs.query({url: "*://*.hh.ru/search/*"}, (tabs) => {
                if (!tabs.length) {
                    if (alert) {
                        alert("Вкладка поиска не найдена.");
                    }

                    throw new Error("Вкладка поиска не найдена.");
                }

                // сообщение посылается только первой вкладке.
                // на данный обработка информации с нескольких вкладок не реализована.
                chrome.tabs.sendMessage(tabs[0].id, message, (response) => {
                    return resolve(response);
                });
            });
        })
    }
}


/**
 * Управление пользовательскими настройками.
 */
class Settings {
    /**
     * Инициализация настроек.
     * 
     * Получает настройки и устанавливает их в `USER_SETTINGS`.
     * 
     * @static
     * @async
     * 
     * @returns {Promise<void>}
     * Promise, который будет выполнен, когда настройки будут получены.
     */
    static init() {
        return new Promise((resolve) => {
            chrome.storage.sync.get(null, (items) => {
                USER_SETTINGS = items;
                return resolve();
            })
        });
    }

    /**
     * Сохраняет настройки из `USER_SETTINGS`.
     * 
     * @static
     * @async
     * 
     * @returns {Promise<void>}
     * Promise, который будет выполнен, когда настройки будут сохранены.
     */
    static save() {
        return new Promise((resolve) => {
            chrome.storage.sync.set(USER_SETTINGS, () => {
                return resolve();
            });
        });
    }
}


/**
 * Содержит информацию о кнопках на странице.
 */
class Buttons {
    /**
     * Кнопки управления: старт, пауза, стоп.
     */
    static get controlButtons() {
        return [
            {id: "start", isDisabled: false, events: [{type: "click", method: () => {Buttons.startButtonMethod()}}]},
            {id: "pause", isDisabled: true, events: [{type: "click", method: () => {Buttons.pauseButtonMethod()}}]},
            {id: "stop", isDisabled: true, events: [{type: "click", method: () => {Buttons.stopButtonMethod()}}]}
        ];
    }

    /**
     * Кнопки таблицы: раскрыть, xlsx, csv.
     */
    static get tableButtons() {
        return [
            {id: "expand", isDisabled: false, events: [{type: "click", method: () => {Buttons.expandButtonMethod()}}]},
            {id: "xlsx", isDisabled: false, events: [{type: "click", method: () => {Main.saveTable("xlsx")}}]},
            {id: "csv", isDisabled: false, events: [{type: "click",  method: () => {Main.saveTable("csv")}}]}
        ];
    }

    /**
     * Метод для кнопки "Старт".
     * 
     * Если парсинг уже был начат, то посылает команду о
     * возобновлении парсинга, иначе посылает команду о начале парсинга.
     * Делает кнопку "Старт" неактивной, а все остальные - активными.
     * 
     * @static
     * @async
     */
    static async startButtonMethod() {
        const stopButton = document.getElementById("stop");

        // если кнопка "Стоп" выключена, то парсинг не был начат.
        if (stopButton.hasAttribute("disabled")) {
            await Message.sendMessage({type: "command", command: "startParsing"});
        } else {
            await Message.sendMessage({type: "command", command: "resumeParsing"});
        }

        PARSING_IS_ENABLED = true;

        for (let button of Buttons.controlButtons) {
            const element = document.getElementById(button.id);

            if (button.id === "start") {
                element.setAttribute("disabled", "disabled");
            } else {
                element.removeAttribute("disabled");
            }
        }
    }

    /**
     * Метод для кнопки "Пауза".
     * 
     * Посылает команду о приостановлении парсинга и
     * делает кнопку "Пауза" неактивной, а все остальные - активными.
     * 
     * @static
     * @async
     */
    static async pauseButtonMethod() {
        await Message.sendMessage({type: "command", command: "pauseParsing"});

        for (let button of Buttons.controlButtons) {
            const element = document.getElementById(button.id);

            if (button.id === "pause") {
                element.setAttribute("disabled", "disabled");
            } else {
                element.removeAttribute("disabled");
            }
        }
    }

    /**
     * Метод для кнопки "Стоп".
     * 
     * Посылает команду об остановке парсинга и
     * возвращает кнопки в изначальное состояние.
     * 
     * @static
     * @async
     */
    static async stopButtonMethod() {
        await Message.sendMessage({type: "command", command: "stopParsing"});
        this.resetControlButtons();
    }

    /**
     * Возвращает кнопки управления в изначальное состояние:
     * 
     * Старт - активна, остальные - неактивны.
     * 
     * @static
     */
    static resetControlButtons() {
        PARSING_IS_ENABLED = false;

        for (let button of Buttons.controlButtons) {
            const element = document.getElementById(button.id);

            if (button.id === "start") {
                element.removeAttribute("disabled");
            } else {
                element.setAttribute("disabled", "disabled");
            }
        }
    }

    /**
     * Метод для кнопки "Раскрыть".
     * 
     * Таблица будет отображать весь контент.
     * 
     * @static
     */
    static expandButtonMethod() {
        const button = document.getElementById("expand");
        const table = document.getElementById("table-resume");

        if (table.classList.contains("custom-table-hide-overflow")) {
            table.classList.remove("custom-table-hide-overflow");
            button.innerText = "Скрыть";
        } else {
            table.classList.add("custom-table-hide-overflow");
            button.innerText = "Раскрыть";
        }
    }
}


/**
 * Содержит информацию о формах на странице.
 * 
 * Значения форм сохраняются автоматически при вводе.
 */
class Forms {
    /**
     * Формы ввода.
     */
    static get forms() {
        return [
            {id: "fileName", settingId: "fileName", settingType: "string", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]},
            {id: "openPagesInNewTab", settingId: "openPagesInNewTab", settingType: "boolean", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]},
            {id: "timeBetweenPageOpening", settingId: "timeBetweenPageOpening", settingType: "number", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]},
            {id: "multithread", settingId: "multithread", settingType: "boolean", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]},
            {id: "threadCount", settingId: "threadCount", settingType: "number", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]},
            {id: "pagination", settingId: "pagination", settingType: "boolean", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]},
            {id: "checkForDuplicate", settingId: "checkForDuplicate", settingType: "boolean", isDisabled: false, events: [{type: "input", method: () => {Forms.inputMethod()}}]}
        ];
    }

    /**
     * Вызывается, когда значение формы изменяется.
     * 
     * Сохраняет настройки и отправляет команду для обновления настроек.
     * 
     * @static
     * @async
     */
    static async inputMethod() {
        await this.saveSettings();
        await Message.sendMessage({type: "command", command: "updateSettings"}, false);
    }

    /**
     * Получает значения с форм и сохраняет их в настройки.
     * 
     * @static
     * @async
     */
    static async saveSettings() {
        for (let form of this.forms) {
            const element = document.getElementById(form.id);

            switch (form.settingType) {
                case "string": {
                    USER_SETTINGS[form.settingId] = String(element.value);
                    break;
                }

                case "boolean": {
                    USER_SETTINGS[form.settingId] = Boolean(element.checked);
                    break;
                }

                case "number": {
                    USER_SETTINGS[form.settingId] = Number(element.value);
                    break;
                }
            
                default: {
                    throw new Error("Неизвестный тип.");
                }
            }
        }

        await Settings.save();
    }
}


/**
 * Управляет общими процессами.
 */
class Main {
    /**
     * Вызывается, когда страница имеет статус "DOMContentLoaded".
     * 
     * @static
     * @async
     */
    static async main() {
        this.initElements(Buttons.controlButtons);
        this.initElements(Buttons.tableButtons);
        M.AutoInit(); // инициализация элементов materialize фреймворка.
        await Settings.init(); // перед инициализацией форм нужно дождаться загрузки настроек.
        this.initElements(Forms.forms);
    }

    /**
     * Инициализирует кнопки.
     * 
     * @static
     * 
     * @param {Array<{id: string, isDisabled: boolean, events: Array<{type: string, method: () => any}>, settingId?: string, settingType?: srting}>} elements 
     * Кнопки для инициализации. 
     */
    static initElements(elements) {
        for (let element of elements) {
            const elmnt = document.getElementById(element.id);

            if (element.isDisabled) {
                elmnt.setAttribute("disabled", "disabled");
            } else {
                elmnt.removeAttribute("disabled");
            }

            for (let event of element.events) {
                elmnt.addEventListener(event.type, event.method);
            }

            // это форма.
            if (element.settingId) {
                if (element.settingType === "boolean") {
                    elmnt.checked = USER_SETTINGS[element.settingId];
                } else {
                    elmnt.value = USER_SETTINGS[element.settingId];
                }
            }
        }
    }

    /**
     * Присоединяет данные к таблице.
     * 
     * Ожидаются данные от `/interaction/content/parser.js`.
     * 
     * @static
     * 
     * @param {Object} data Объект данных для присоединения.
     */
    static appendDataToTable(data) {
        const tableBody = document.getElementById("table-resume-body");
        const tr = document.createElement("tr");

        const td1 = document.createElement("td");
        td1.innerText = data["ФИО"];
        tr.appendChild(td1);

        const td2 = document.createElement("td");
        td2.innerText = data["Пол"];
        tr.appendChild(td2);

        const td3 = document.createElement("td");
        td3.innerText = data["Возраст"];
        tr.appendChild(td3);

        const td4 = document.createElement("td");
        td4.innerText = data["Город"];
        tr.appendChild(td4);

        const td5 = document.createElement("td");
        td5.innerText = data["Желаемая должность"];
        tr.appendChild(td5);

        const td6 = document.createElement("td");
        td6.innerText = data["Желаемая зарплата"];
        tr.appendChild(td6);

        const td7 = document.createElement("td");
        td7.innerText = data["Опыт работы"];
        tr.appendChild(td7);

        const td8 = document.createElement("td");
        td8.innerText = data["Образование"];
        tr.appendChild(td8);

        const td9 = document.createElement("td");
        td9.innerText = data["Ключевые навыки"];
        tr.appendChild(td9);

        const td10 = document.createElement("td");
        td10.innerText = data["Знание языков"];
        tr.appendChild(td10);

        const td11 = document.createElement("td");
        td11.innerText = data["Обо мне"];
        tr.appendChild(td11);

        const td12 = document.createElement("td");
        td12.innerText = data["Телефон"];
        tr.appendChild(td12);

        const td13 = document.createElement("td");
        td13.innerText = data["Почта"];
        tr.appendChild(td13);

        const td14 = document.createElement("td");
        td14.innerText = data["URL"];
        tr.appendChild(td14);

        tableBody.appendChild(tr);
    }

    /**
     * Сохраняет данные с помощью SheetJS.
     * 
     * @static
     * 
     * @param {Array<Object>} data Данные для таблицы.
     * @param {string} name Имя таблицы.
     * @param {string} format Формат таблицы.
     */
    static saveData(data, name, format) {
        // создаем worksheet.
        const ws = XLSX.utils.json_to_sheet(data);
    
        // добавляем в workbook.
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Резюме");
    
        // создаем и сохраняем файл.
        // формат: либо xlsx, либо csv.
        XLSX.writeFile(wb, `${name}.${format}`);
    }

    /**
     * Сохраняет данные из глобальной переменной `DATA`.
     * 
     * Имя берется с `input (#fileName)`.
     * 
     * @static
     * 
     * @param {string} format 
     * Формат файла. Ожидается либо `xlsx`, либо `csv`.
     */
    static saveTable(format) {
        const fixFileName = (name, char) => {
            return name.replace(/[\\\/\:\*\?\"\<\>\|]/g, char || "");
        }

        let name = document.getElementById("fileName").value;
        name = fixFileName(name) || " ";

        this.saveData(DATA, name, format);
    }
}


/**
 * Обработчик сообщений.
 * 
 * Может обрабатывать сообщения следующих типов: "command".
 */
class OnMessage {
    /**
     * Обрабатывает все сообщения и 
     * перенаправляет их по `type`.
     * 
     * @static
     * 
     * @see https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage#Parameters
     */
    static onMessage(request, sender, sendResponse) {
        switch (request.type) {
            case "command": {
                this.commandHandler(request, sender, sendResponse);
                break;
            }
        
            default: {
                console.warn(request);
                console.warn(sender);
                throw new Error("Неизвестный тип сообщения.");
            }
        }
    }

    /**
     * Обрабатывает сообщения типа `command`.
     * 
     * @static
     */
    static commandHandler(request, sender, sendResponse) {
        switch (request.command) {
            case "readyForParsing": {
                // включена пагинация.
                if (PARSING_IS_ENABLED) {
                    Message.sendMessage({type: "command", command: "startParsing"});
                }

                break;
            }

            case "collectData": {
                OnMessage.collectData(request.data);
                break;
            }

            case "parsingCompleted": {
                Buttons.resetControlButtons();
                PARSING_IS_ENABLED = false;
                console.log("Парсинг завершен.");
                break;
            }

            case "detachParsing": {
                Buttons.resetControlButtons();
                PARSING_IS_ENABLED = false;
                break;
            }

            // Новая вкладка (`*://*.hh.ru/resume/*`) прислала данные после парсинга.
            // Обрабатываем это точно также, как если бы получили данные от `*://*.hh.ru/search/*`.
            // Посылаем вкладке (`*://*.hh.ru/resume/*`) команду, что можно продолжать работу.
            case "newTabWasParsed": {
                Message.sendMessage({type: "command", command: "newTabWasParsedCanContinue"})
                OnMessage.collectData(request.data);
                break;
            }
        
            default: {
                console.warn(request);
                console.warn(sender);
                throw new Error("Неизвестная команда.");
            }
        }
    }

    /**
     * Сохраняем данные парсинга и добавляем их в таблицу.
     * 
     * @static
     * 
     * @param {Object} data Присланные данные. 
     */
    static collectData(data) {
        const _save = (item) => {
            DATA.push(item);
            Main.appendDataToTable(item);
        };

        if (USER_SETTINGS.checkForDuplicate) {
            /**
             * Для проверки на дубликаты нельзя использовать `DATA.includes`.
             * Объекты проверяются по ссылкам, а не по значениям.
             * Два объекта с одинаковыми ключами и значениями не равны друг другу.
             */

            /**
             * Проверка на уникальность осуществяется по URL.
             * Убедитесь, что поддомены всегда одинаковы!
             */

            if (!DATA.find((element) => {return element.URL === data.URL})) {
                _save(data);
            }
        } else {
            _save(data);
        }

        console.log(data);
    }
}


if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", Main.main);
} else {
    Main.main();
}

// слушаем сообщения от контент-скрипта на HH.
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    OnMessage.onMessage(request, sender, sendResponse);
    // выключено, так как мы не посылаем ответ.
    //return true; // асинхронный ответ.
});
